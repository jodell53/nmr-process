# nmr-process

Description: Restart of work on Dan Yelle's 2D NMR data

Goal: Construct a fully open-source Python and/or R tool chain to apply chemoinformatics to wood lignin 2D NMR data


**Task groups:**   
- set up Docker container with tools  
- Read data
- massage into form suitable for OPLS
- 
 
Tools in Python
- conda and packages numpy/scipy/matplotlib
- Jupyter package turns out to be needed for interactive visualization
- nmrglue package installed through pip becasue conda packages are out of date and/or require older version of Python


