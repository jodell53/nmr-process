total 32
drwxr-sr-x 5 jovyan users 4096 Feb 28 20:46 .
drwsrwsr-x 1 jovyan users 4096 Feb 28 20:16 ..
-rw-r--r-- 1 jovyan users    0 Feb 28 20:46 dirlist.txt
drwxr-sr-x 8 jovyan users 4096 Feb 28 19:40 .git
-rw-r--r-- 1 jovyan users 1713 Feb 28 19:40 .gitignore
drwxr-sr-x 4 jovyan users 4096 Feb 28 19:40 jupyter-nmr
-rw-r--r-- 1 jovyan users 1479 Feb 28 19:40 LICENSE.md
drwxr-sr-x 3 jovyan users 4096 Feb 28 19:40 miniconda-nmr
-rw-r--r-- 1 jovyan users  555 Feb 28 19:40 README.md
