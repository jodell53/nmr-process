##TITLE= Audit trail, TOPSPIN		Version 3.2
##JCAMPDX= 5.01
##ORIGIN= Bruker BioSpin GmbH
##OWNER= dyelle
$$ C:/Users/dyelle/Desktop/Chemometrics/L_E data/4-L/12/pdata/1/auditp.txt
##AUDIT TRAIL=  $$ (NUMBER, WHEN, WHO, WHERE, PROCESS, VERSION, WHAT)
(   1,<2012-07-07 07:18:26.331 -0500>,<dyelle>,<rotvieh>,<go>,<TOPSPIN 3.0>,
      <created by zg
	started at 2012-07-07 02:43:10.319 -0500,
	POWCHK enabled, PULCHK disabled,
       configuration hash MD5:
       58 DF 7E 16 D0 99 20 EE 7B C1 FB D0 85 18 D0 76
       data hash MD5: 2800 * 560
       68 08 EF 9B 4D 29 57 E1 76 C9 CA 7B 01 3C 9F 71>)
(   2,<2014-04-21 16:50:51.273 -0500>,<DS\dyelle>,<LTHP2290Q49>,<proc2d>,<TOPSPIN 3.2>,
      <Start of raw data processing
       xfb F2: SI = 1K WDW = 2 GB = 0.001 LB = -0.1 FT_mod = 6 PKNL = 1 PHC0 = 87.98438 PHC1 = -40 F1: SI = 1K WDW = 4 SSB = 2 FT_mod = 6 FCOR = 0.5 PHC0 = -5.196094 PHC1 = 7.2
       data hash MD5: 1K * 1K
       7D 31 71 C3 B8 17 E4 BD D3 40 73 E6 C8 F6 39 5A>)
(   3,<2014-04-21 16:51:02.318 -0500>,<DS\dyelle>,<LTHP2290Q49>,<proc2d>,<TOPSPIN 3.2>,
      <abs2 F2: ABSG = 5 ABSF1 = 9.988889 ABSF2 = -0.0132552
       data hash MD5: 1K * 1K
       C0 A7 2D 63 FB 78 7C EA 4B 20 51 4B 5B 20 9B F6>)
(   4,<2014-04-21 16:51:03.738 -0500>,<DS\dyelle>,<LTHP2290Q49>,<proc2d>,<TOPSPIN 3.2>,
      <abs1 F1: ABSG = 5 ABSF1 = 200.01 ABSF2 = -0.01000549
       data hash MD5: 1K * 1K
       BB 35 60 6D 1D 3C ED 1F 66 06 4D 20 D9 D7 2A 66>)
(   5,<2014-04-21 16:51:50.132 -0500>,<DS\dyelle>,<LTHP2290Q49>,<proc2d>,<TOPSPIN 3.2>,
      <pk1 fgphup F1: PHC0 = -15 PHC1 = 0
       data hash MD5: 1K * 1K
       58 71 F0 06 8D 2C 48 DE 08 68 DA 41 E5 A0 5E 64>)
(   6,<2014-04-21 16:52:30.848 -0500>,<DS\dyelle>,<LTHP2290Q49>,<audit>,<TOPSPIN 3.2>,
      <user comment:
       TITLE=51BN09LTITLE_END
       data hash MD5: 1K * 1K
       58 71 F0 06 8D 2C 48 DE 08 68 DA 41 E5 A0 5E 64>)
(   7,<2014-04-21 16:52:33.765 -0500>,<DS\dyelle>,<LTHP2290Q49>,<audit>,<TOPSPIN 3.2>,
      <user comment:
       TITLE=51BN09LTITLE_END
       data hash MD5: 1K * 1K
       58 71 F0 06 8D 2C 48 DE 08 68 DA 41 E5 A0 5E 64>)
(   8,<2014-07-18 10:03:20.367 -0500>,<DS\dyelle>,<LTHP2290Q49>,<proc2d>,<TOPSPIN 3.2>,
      <pk1 fgphup F1: PHC0 = 50.16094 PHC1 = -69.6
       data hash MD5: 1K * 1K
       DD 09 18 59 24 39 33 FD B0 4C C0 2A 2A F9 65 DB>)
(   9,<2014-07-18 10:03:20.710 -0500>,<DS\dyelle>,<LTHP2290Q49>,<proc2d>,<TOPSPIN 3.2>,
      <pk1 fgphup F1: PHC0 = 29.96485 PHC1 = -62.4
       data hash MD5: 1K * 1K
       5F 68 76 9C 9D 31 93 50 8E 15 AA D4 FB 77 7C 8A>)
(  10,<2014-07-18 10:04:11.754 -0500>,<DS\dyelle>,<LTHP2290Q49>,<proc2d>,<TOPSPIN 3.2>,
      <pk1 fgphup F1: PHC0 = -125.6578 PHC1 = 196
       data hash MD5: 1K * 1K
       CA E6 64 65 AD 97 54 A4 9B 8C B1 23 D5 1A AD 2F>)
(  11,<2014-07-18 10:04:50.162 -0500>,<DS\dyelle>,<LTHP2290Q49>,<proc2d>,<TOPSPIN 3.2>,
      <pk1 fgphup F1: PHC0 = 38.05312 PHC1 = -52.8
       data hash MD5: 1K * 1K
       A0 8A 3E 45 BD A8 63 DE 18 B0 CA CF 97 32 F9 CE>)
##END=

$$ hash MD5
$$ 7E FC 3A EA FA AC 72 27 EA B7 1B F6 FE 09 40 64
