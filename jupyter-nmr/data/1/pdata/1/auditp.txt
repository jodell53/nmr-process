##TITLE= Audit trail, TOPSPIN		Version 3.2
##JCAMPDX= 5.01
##ORIGIN= Bruker BioSpin GmbH
##OWNER= dyelle
$$ C:/Users/dyelle/Desktop/Chemometrics/L_E data/1-E/12/pdata/1/auditp.txt
##AUDIT TRAIL=  $$ (NUMBER, WHEN, WHO, WHERE, PROCESS, VERSION, WHAT)
(   1,<2012-07-06 17:15:16.900 -0500>,<dyelle>,<rotvieh>,<go>,<TOPSPIN 3.0>,
      <created by zg
	started at 2012-07-06 12:40:00.883 -0500,
	POWCHK enabled, PULCHK disabled,
       configuration hash MD5:
       58 DF 7E 16 D0 99 20 EE 7B C1 FB D0 85 18 D0 76
       data hash MD5: 2800 * 560
       AC 16 89 0A ED 4F 75 54 C5 AD 2C 70 5E C7 BF 06>)
(   2,<2014-04-21 16:34:54.219 -0500>,<DS\dyelle>,<LTHP2290Q49>,<proc2d>,<TOPSPIN 3.2>,
      <Start of raw data processing
       xfb F2: SI = 1K WDW = 2 GB = 0.001 LB = -0.1 FT_mod = 6 PKNL = 1 PHC0 = 87.98438 PHC1 = -40 F1: SI = 1K WDW = 4 SSB = 2 FT_mod = 6 FCOR = 0.5 PHC0 = -5.196094 PHC1 = 7.2
       data hash MD5: 1K * 1K
       F5 EC CC 54 C8 D1 12 71 6E EB CB 1C 2C 5E EF 52>)
(   3,<2014-04-21 16:36:34.044 -0500>,<DS\dyelle>,<LTHP2290Q49>,<proc2d>,<TOPSPIN 3.2>,
      <pk1 fgphup F1: PHC0 = -26.28437 PHC1 = 17.6
       data hash MD5: 1K * 1K
       8B 46 AF CA 5C 4A 2E 2A E2 E1 21 4B D8 BB 93 C5>)
(   4,<2014-04-21 16:38:28.213 -0500>,<DS\dyelle>,<LTHP2290Q49>,<audit>,<TOPSPIN 3.2>,
      <user comment:
       TITLE=51BN03ETITLE_END
       data hash MD5: 1K * 1K
       8B 46 AF CA 5C 4A 2E 2A E2 E1 21 4B D8 BB 93 C5>)
(   5,<2014-04-21 16:38:30.662 -0500>,<DS\dyelle>,<LTHP2290Q49>,<audit>,<TOPSPIN 3.2>,
      <user comment:
       TITLE=51BN03ETITLE_END
       data hash MD5: 1K * 1K
       8B 46 AF CA 5C 4A 2E 2A E2 E1 21 4B D8 BB 93 C5>)
##END=

$$ hash MD5
$$ 8E 52 4A D4 BC 2F A4 DE D2 08 87 A8 B6 F2 97 C7
