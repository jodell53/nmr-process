%Convert ppm values for spectrum axes to corresponding data points ranges in x
%and y

function [x_lim,y_lim]=ppm_to_dp(SI2,SI1,F2_ppm,F1_ppm,limits)

scale_x=SI2/(F2_ppm(1,1)-F2_ppm(end));
scale_y=SI1/(F1_ppm(end)-F1_ppm(1,1));
x1=round(scale_x*(F2_ppm(1,1)-limits(1,2)));
x2=round(scale_x*(F2_ppm(1,1)-limits(1,1)));
y1=round(scale_y*(limits(1,3)-F1_ppm(1,1)));
y2=round(scale_y*(limits(1,4)-F1_ppm(1,1)));
x_lim=x1:x2;
y_lim=y1:y2;

end