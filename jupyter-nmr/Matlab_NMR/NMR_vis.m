function varargout = NMR_vis(varargin)

% Copyright (c) Mattias Hedenström, 2008, 2009, 2010 (mattias.hedenstrom@chem.umu.se)
%
%    NMR_vis is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    This program is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%    Version 1.0 Release data: 2010-05-17

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @NMR_vis_OpeningFcn, ...
                   'gui_OutputFcn',  @NMR_vis_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before NMR_vis is made visible.
function NMR_vis_OpeningFcn(hObject, eventdata, handles, varargin)

% Choose default command line output for NMR_vis
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% --- Outputs from this function are returned to the command line.
function varargout = NMR_vis_OutputFcn(hObject, eventdata, handles) 

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
function file_menu_Callback(hObject, eventdata, handles)


% --------------------------------------------------------------------
function file_menu_project_Callback(hObject, eventdata, handles)

folder=uigetdir('E:\Jobb\','Select Folder');
set(handles.Path,'string',folder);
cd (folder);
load settings.mat

if isfield(setting,'merge') == 1
    nr = 1 : size(setting.merge,1);
else
    nr = 1 : setting.incl_nr;
end
set(handles.Spectralist,'string',nr);


% --------------------------------------------------------------------
function file_menu_export_Callback(hObject, eventdata, handles)
load settings.mat;
load current_data.mat;

export=p;
export=rot90(export,3);
export=reshape(export,setting.dp(1)*setting.dp(2),1);
export=export*1000000; %to get the same dynamic range as original sepctrum

%save 2rr file
folder=uigetdir(setting.path,'Select Folder');
file=(fullfile(folder,'2rr'));
if exist(file)==2;
    button = questdlg('Overwrite existing spectrum?','Warning');
    if char(button)==lower('yes');
        fid=fopen(file,'w','l');
        fwrite(fid,export,'int32');
        fclose(fid);
    end
end
if exist(file)==0;
    fid=fopen(file,'w','l');
    fwrite(fid,export,'int32');
    fclose(fid);
end


% --------------------------------------------------------------------
function file_menu_quit_Callback(hObject, eventdata, handles)
delete current_data.mat;
close


% --- Executes on selection change in Spectralist.
function Spectralist_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function Spectralist_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Plot plot spectrum.
function Plot_Callback(hObject, eventdata, handles)
load('loadings.mat');
load('settings.mat');

F2_ppm=setting.F2ppm;
F1_ppm=setting.F1ppm;

region=get(handles.Spectralist,'value');        %read which region to be plotted
p_ROI = eval(['loading.roi' num2str(region)]);  %extract the relevant loadings
comp = get(handles.listbox4,'value');           %read the component to be plotted


maxint=max(p_ROI(comp,:));
minint=min(p_ROI(comp,:));
V=[minint:(maxint-minint)/20:maxint];
p = reshape(p_ROI(comp,:),setting.dp(2),setting.dp(1));
if get(handles.Lockbutton,'value')==1
    limits=axis;
    scale_x=setting.dp(1)/(F2_ppm(1,1)-F2_ppm(end));
    scale_y=setting.dp(2)/(F1_ppm(end)-F1_ppm(1,1));
    x1=round(scale_x*(F2_ppm(1,1)-limits(1,2)));
    x2=round(scale_x*(F2_ppm(1,1)-limits(1,1)));
    y1=round(scale_y*(limits(1,3)-F1_ppm(1,1)));
    y2=round(scale_y*(limits(1,4)-F1_ppm(1,1)));
    x_lim=x1:x2;
    y_lim=y1:y2;

    if min(x_lim)<1;
        x_lim=1:setting.dp(1);
        y_lim=1:setting.dp(2);
    end
    h=contour(F2_ppm(x_lim),F1_ppm(y_lim),p(y_lim,x_lim),V);

else
    contour(F2_ppm,F1_ppm,p,V);
end
    set(gca,'XDir','reverse')
    set(gca,'YDir','reverse')
    xlabel('ppm');
    ylabel('ppm');

save('current_data.mat','p','V','region','comp');


% --- Executes on button press in increasebutton. Higher Intensity.
function increasebutton_Callback(hObject, eventdata, handles)

load current_data.mat
load settings.mat
p = p*2;
F2_ppm=setting.F2ppm;
F1_ppm=setting.F1ppm;
if get(handles.Lockbutton,'value')==1
    limits=axis;
    scale_x=setting.dp(1)/(F2_ppm(1,1)-F2_ppm(end));
    scale_y=setting.dp(2)/(F1_ppm(end)-F1_ppm(1,1));
    x1=round(scale_x*(F2_ppm(1,1)-limits(1,2)));
    x2=round(scale_x*(F2_ppm(1,1)-limits(1,1)));
    y1=round(scale_y*(limits(1,3)-F1_ppm(1,1)));
    y2=round(scale_y*(limits(1,4)-F1_ppm(1,1)));
    x_lim=x1:x2;
    y_lim=y1:y2;

    if min(x_lim)<1;
        x_lim=1:setting.dp(1);
        y_lim=1:setting.dp(2);
    end
    h=contour(F2_ppm(x_lim),F1_ppm(y_lim),p(y_lim,x_lim),V);

else
    contour(F2_ppm,F1_ppm,p,V);
end
    set(gca,'XDir','reverse')
    set(gca,'YDir','reverse')
    xlabel('ppm');
    ylabel('ppm');

save('current_data.mat','p','V','region','comp');


% --- Executes on button press in decreasebutton. Lower intensity.
function decreasebutton_Callback(hObject, eventdata, handles)

load current_data.mat
load settings.mat
p = p/2;
F2_ppm=setting.F2ppm;
F1_ppm=setting.F1ppm;
if get(handles.Lockbutton,'value')==1
    limits=axis;
    scale_x=setting.dp(1)/(F2_ppm(1,1)-F2_ppm(end));
    scale_y=setting.dp(2)/(F1_ppm(end)-F1_ppm(1,1));
    x1=round(scale_x*(F2_ppm(1,1)-limits(1,2)));
    x2=round(scale_x*(F2_ppm(1,1)-limits(1,1)));
    y1=round(scale_y*(limits(1,3)-F1_ppm(1,1)));
    y2=round(scale_y*(limits(1,4)-F1_ppm(1,1)));
    x_lim=x1:x2;
    y_lim=y1:y2;

    if min(x_lim)<1;
        x_lim=1:setting.dp(1);
        y_lim=1:setting.dp(2);
    end
    h=contour(F2_ppm(x_lim),F1_ppm(y_lim),p(y_lim,x_lim),V);

else
    contour(F2_ppm,F1_ppm,p,V);
end
    set(gca,'XDir','reverse')
    set(gca,'YDir','reverse')
    xlabel('ppm');
    ylabel('ppm');

save('current_data.mat','p','V','region','comp');


% --- Executes on button press in All.
function All_Callback(hObject, eventdata, handles)

load current_data.mat
load settings.mat
F2_ppm=setting.F2ppm;
F1_ppm=setting.F1ppm;
h=contour(F2_ppm,F1_ppm,p,V);
set(gca,'XDir','reverse')
set(gca,'YDir','reverse')
xlabel('ppm');
ylabel('ppm');

save('current_data.mat','p','V','region','comp');


% --- Executes on button press in Cursorbutton.
function Cursorbutton_Callback(hObject, eventdata, handles)

datacursormode
set(handles.Zoombutton,'value',0);


% --- Executes on button press in Zoombutton.
function Zoombutton_Callback(hObject, eventdata, handles)

zoom on
set(handles.Cursorbutton,'value',0);


% --- Executes on button press in Lockbutton.
function Lockbutton_Callback(hObject, eventdata, handles)


% --- Executes on button press in backscaleradio.
function backscaleradio_Callback(hObject, eventdata, handles)


% --- Executes on selection change in listbox4.
function listbox4_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function listbox4_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in import.
function import_Callback(hObject, eventdata, handles)

folder=get(handles.Path,'string');
cd (folder);
if exist('loadings.mat')==2;
    load loadings.mat;
end
load settings.mat;
region=get(handles.Spectralist,'value');
[filename, pathname, filterindex] = uigetfile('*.mat', 'Pick a mat-file');
load(fullfile(folder,filename));

loadingvector=zeros(1,setting.dp_final(region,3));
loadingvector=loadingvector';
NR_OF_LOADINGS = size(DATA,2);
list_comp = 1 : NR_OF_LOADINGS;
set(handles.listbox4,'string',list_comp);
DATA_temp=[];
id=[];
p=[];
p_uv=[];

if get(handles.backscaleradio,'value')==1;
    DATA_UV=DATA;
    if isfield(setting,'merge') == 1
        std_dev = zeros(size(DATA,1),1);
        std_dev = std_dev';
        start = 1;
        for i = 1 : setting.merge(region,2);
            temp = eval(['setting.std' num2str(i)]);
            std_dev(start:(start-1+size(temp,2)))=temp;
            start = start+size(temp,2);
        end
    else
    std_dev=eval(['setting.std' num2str(region)]);
    end
    for j=1:NR_OF_LOADINGS;
        p1=DATA(:,j);
        p1_scaled=p1.*std_dev';
        DATA_temp=[DATA_temp p1_scaled];
    end
    DATA=DATA_temp;
end
B=zeros(setting.dp(2),setting.dp(1));
if isfield(setting,'merge') == 1

    for j=1:NR_OF_LOADINGS
        start = 1;
        B=zeros(setting.dp(2),setting.dp(1));
        for i = 1 : setting.merge(region,1) : setting.merge(region,2);
            temp = eval(['setting.dp_afternoise' num2str(i)]);
            length = temp(2);
            DATA_ROI = DATA(start:(start+length-1),:);
            DATA1=[eval(['setting.Index' num2str(i)])' DATA_ROI]; %extract merged ROI
            start = start+length;

            loadingvector = zeros(1,setting.dp_final(i,3));
            loadingvector=loadingvector';
            loadingvector(DATA1(:,1))=DATA1(:,j+1); %insert ROI in full length vector
            p_temp = loadingvector';
            p_roi = reshape(p_temp(1,:),setting.dp_final(i,2),setting.dp_final(i,1)); %reshape into matrix
            B(eval(['setting.R' num2str(i)]),eval(['setting.K' num2str(i)]))=p_roi; %place loading into full spectrum B
        end
        vector = reshape(B,1,setting.dp(3)); %full-size loadingspectrum into vector
        p = [p ; vector];
    end
    eval(['loading.roi' num2str(region) '= p']);
    
    %do the same thing for UV-scaled loadings
    if get(handles.backscaleradio,'value')==1;
        for j=1:NR_OF_LOADINGS
            start = 1;
            B=zeros(setting.dp(2),setting.dp(1));
            for i = 1 : setting.merge(region,1) : setting.merge(region,2);
                temp = eval(['setting.dp_afternoise' num2str(i)]);
                length = temp(2);
                DATA_ROI = DATA_UV(start:(start+length-1),:);
                DATA1_UV=[eval(['setting.Index' num2str(i)])' DATA_ROI]; %extract merged ROI
                start = start+length;

                loadingvector = zeros(1,setting.dp_final(i,3));
                loadingvector=loadingvector';
                loadingvector(DATA1_UV(:,1))=DATA1_UV(:,j+1); %insert ROI in full length vector
                p_temp = loadingvector';
                p_roi = reshape(p_temp(1,:),setting.dp_final(i,2),setting.dp_final(i,1)); %reshape into matrix
                B(eval(['setting.R' num2str(i)]),eval(['setting.K' num2str(i)]))=p_roi; %place loading into full spectrum B
            end
            vector_uv = reshape(B,1,setting.dp(3)); %full-size loadingspectrum into vector
            p_uv = [p_uv ; vector_uv];
        end
        eval(['loading.roi_uv' num2str(region) '= p_uv']);
        
    end
else
    
    DATA1=[eval(['setting.Index' num2str(region)])' DATA];
    for j=1:NR_OF_LOADINGS
        loadingvector = zeros(1,setting.dp_final(region,3));
        loadingvector=loadingvector';
        loadingvector(DATA1(:,1))=DATA1(:,j+1); %insert ROI in full length vector
        p_temp = loadingvector';
        p_roi = reshape(p_temp(1,:),setting.dp_final(region,2),setting.dp_final(region,1)); %reshape into matrix
        B(eval(['setting.R' num2str(region)]),eval(['setting.K' num2str(region)]))=p_roi; %place loading into full spectrum B
        vector = reshape(B,1,setting.dp(3)); %full-size loadingspectrum into vector
        p = [p ; vector];
    end
    eval(['loading.roi' num2str(region) '= p']);

    %again do the same for UV-scaled loadings
    if get(handles.backscaleradio,'value')==1;
        DATA1_UV=[eval(['setting.Index' num2str(region)])' DATA_UV];
        for j=1:NR_OF_LOADINGS
            loadingvector = zeros(1,setting.dp_final(region,3));
            loadingvector=loadingvector';
            loadingvector(DATA1_UV(:,1))=DATA1_UV(:,j+1); %insert ROI in full length vector
            p_temp = loadingvector';
            p_roi = reshape(p_temp(1,:),setting.dp_final(region,2),setting.dp_final(region,1)); %reshape into matrix
            B(eval(['setting.R' num2str(region)]),eval(['setting.K' num2str(region)]))=p_roi; %place loading into full spectrum B
            vector_uv = reshape(B,1,setting.dp(3)); %full-size loadingspectrum into vector
            p_uv = [p_uv ; vector_uv];
        end
        eval(['loading.roi_uv' num2str(region) '= p_uv']);
    end
end
save('loadings.mat','loading');


function corr_up_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function corr_up_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function corr_down_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function corr_down_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on button press in apply_cutoff.
function apply_cutoff_Callback(hObject, eventdata, handles)

load current_data.mat
load loadings.mat
load settings.mat

pcorr_up = str2num(get(handles.corr_up,'string'));
%pcorr_down = str2num(get(handles.corr_down,'string'));
p_mod = p;
%option for plotting only certain pcorr-ranges, NOT DONE!!!
%B_uv=zeros(setting.dp(2),setting.dp(1));
%loadingvector_uv = zeros(1,setting.dp_final(region,3));
%loadingvector_uv = loadingvector_uv';
%loadingvector_uv(DATA1(:,1))=DATA1(:,j+1); %insert ROI in full length vector
p_uv_temp = eval(['loading.roi_uv',num2str(region)]);
p_uv = p_uv_temp(comp,:);
p_roi = reshape(p_uv(1,:),setting.dp(2),setting.dp(1)); %reshape into matrix
%B_uv(eval(['setting.R' num2str(region)]),eval(['setting.K' num2str(region)]))=p_roi; %place loading into full spectrum B
%p(p_roi<pcorr_down)=0;
%p(p_roi>pcorr_up)=0;
p(abs(p_roi)<pcorr_up)=0;

F2_ppm=setting.F2ppm;
F1_ppm=setting.F1ppm;
contour(F2_ppm,F1_ppm,p,V);
set(gca,'XDir','reverse')
set(gca,'YDir','reverse')
xlabel('ppm');
ylabel('ppm');

save('current_data.mat','p','V','region','comp');


% --- Executes on button press in figure.
function figure_Callback(hObject, eventdata, handles)

load current_data.mat
load settings.mat
F2_ppm=setting.F2ppm;
F1_ppm=setting.F1ppm;
figure
contour(F2_ppm,F1_ppm,p,V);
set(gca,'XDir','reverse')
set(gca,'YDir','reverse')
xlabel('ppm');
ylabel('ppm');


function edit14_Callback(hObject, eventdata, handles)
% hObject    handle to corr_down (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of corr_down as text
%        str2double(get(hObject,'String')) returns contents of corr_down as a double


% --- Executes during object creation, after setting all properties.
function edit14_CreateFcn(hObject, eventdata, handles)
% hObject    handle to corr_down (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end







% --------------------------------------------------------------------
function about_menu_Callback(hObject, eventdata, handles)
fid = fopen('About_NMR_proc.txt','r');
str = fread(fid);
a = find(str==13);
str(a)=[];
fclose(fid);
uiwait(msgbox(char(str'),'About','modal'));



