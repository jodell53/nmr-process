function varargout = NMR_proc(varargin)

% Copyright (c) Mattias Hedenström, 2008, 2009, 2010 (mattias.hedenstrom@chem.umu.se)
%
%    NMR_proc is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    This program is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%    Version 1.0 Release date: 2010-05-17

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @NMR_proc_OpeningFcn, ...
                   'gui_OutputFcn',  @NMR_proc_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT




% --- Executes just before NMR_proc is made visible.
function NMR_proc_OpeningFcn(hObject, eventdata, handles, varargin)

% Choose default command line output for NMR_proc
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


% --- Outputs from this function are returned to the command line.
function varargout = NMR_proc_OutputFcn(hObject, eventdata, handles) 

% Get default command line output from handles structure
varargout{1} = handles.output;



% --------------------------------------------------------------------
function file_menu_Callback(hObject, eventdata, handles)


% --------------------------------------------------------------------
function file_menu_project_Callback(hObject, eventdata, handles)

folder=uigetdir('E:\Jobb\','Select Folder');
set(handles.project_folder,'string',folder);
cd (folder);
if exist(fullfile(folder,'settings.mat'))==2;
    button = questdlg('Overwrite existing project?','Warning');
    switch lower(button);
        case {'yes'};
            delete settings.mat
            delete final*.mat
            delete final*.xls
            delete current_data.mat
            delete orig_spect.mat
            delete mod_spect.mat
        otherwise
    end
end


% --------------------------------------------------------------------
function file_menu_data_Callback(hObject, eventdata, handles)

folder=uigetdir('C:\Documents and Settings\mattias.hedenstrom\My Documents\Jobb\nmrdata\data\mattias\nmr','Select Folder');
set(handles.Path,'string',folder);
file=dir(folder);
A={file.name};
str=A(3:end);
[Y,I]=sort(str2num(char(str)));
str=str(I);

files=str;
set(handles.infotext,'string','loading spectra, please wait');
pause(.1);
orig_spect=[];
DATA_NEW=[];
DATA=[];
Vector=[];
final=[];
X=[];
B=[];
ID_NR={};
titles=[];
file_nr=[];

%Start Read and Transform
set(handles.Zoombutton,'Value',0);
set(handles.Lockbutton,'Value',0);


folder=get(handles.Path,'string');

FILE_procs=[folder,'\' char(files(1)) '\pdata\1\procs'];
    FILE_proc2s=[folder,'\' char(files(1)) '\pdata\1\proc2s'];
    

    head0=textread(FILE_procs,'%s');
    SF2=str2num(char(head0(strmatch('##$SF=',head0')+1))); %scaling factor x-axis (Hz)
    SI2=str2num(char(head0(strmatch('##$SI=',head0')+1))); %nr of points x-axis (Hz)
    SW2=str2num(char(head0(strmatch('##$SW_p=',head0')+1))); %length of x-axis
    OS2=str2num(char(head0(strmatch('##$OFFSET=',head0')+1))); %value of first point on x (ppm)
    nc_proc=str2num(char(head0(strmatch('##$NC_proc=',head0')+1))); %scaling of intensity
    head0=textread(FILE_proc2s,'%s');
    SF1=str2num(char(head0(strmatch('##$SF=',head0')+1))); %scaling factor y-axis (Hz)
    SI1=str2num(char(head0(strmatch('##$SI=',head0')+1))); %nr of points y-axis (Hz)
    SW1=str2num(char(head0(strmatch('##$SW_p=',head0')+1))); %length of y-axis
    OS1=str2num(char(head0(strmatch('##$OFFSET=',head0')+1))); %value of first point on y (ppm)

    %calc. axes (ppm)
    F2_ppm=OS2-[1:SI2]/SI2/SF2*SW2; %x-axis
    F1_ppm=OS1-[1:SI1]/SI1/SF1*SW1; %y-axis
    F1_ppm=fliplr(F1_ppm);


for i=1:size(files,2);
    a=char(files(1,i));
    num=str2num(a);
    file_nr(1,i)=num;
end
nr_of_files=size(file_nr,2);

for i=1:nr_of_files
    FILE_PATH_FID=[folder,'\',num2str(file_nr(i)),'\pdata\1\2rr'];
    fid=fopen(FILE_PATH_FID,'r','l');
    data=fread(fid,'int32');
    fclose(fid);
    %End Read

    %Start Save
    FILE_title=[folder,'\',num2str(file_nr(i)),'\pdata\1\title'];
    head0=textread(FILE_title,'%s');
    title=head0(1);
    titles=[titles title];
    data=data';
    data=data*(2^nc_proc);
    orig_spect=[orig_spect;data];
end
    save('orig_spect.mat','orig_spect');
    save('PROC_DATA.mat','SI2','SI1','F2_ppm','F1_ppm','titles','file_nr');
    set(handles.Spectralist,'string',file_nr);

    %End Save

set(handles.infotext,'string','');
plot(1,1,'W*');
h=text(1,1,'NMR-DATA');
set(h,'HorizontalAlignment','center');
set(h,'HorizontalAlignment','center');
set(h,'Fontsize',30);


% --------------------------------------------------------------------
function file_menu_save_Callback(hObject, eventdata, handles)

folder=get(handles.project_folder,'string');

logfile=fullfile(folder,'logfile.txt');
xlsfile=fullfile(folder,'final.xls');
if exist(xlsfile)==2;
    delete(xlsfile);
end
if exist(logfile)==2;
    delete(logfile);
end
load('mod_spect.mat');
load('PROC_DATA.mat');
load('settings.mat');

setting.time=datestr(now);
setting.path=get(handles.Path,'string');
setting.nr=size(file_nr);

if exist('incl.mat')==2
    load('incl.mat');
    setting.incl=incl_roi_ppm;
end
if exist('excl.mat')==2;
    load('excl.mat');
    setting.excl=excl_roi_ppm;
end
setting.dp=[SI2 SI1 SI2*SI1];
setting.dp_final=[];
if isfield(setting,'align')==0;
    setting.align='NO';
end
textstr=(['Project created: ' char(setting.time)]);
do_log(folder,textstr);
textstr=(['Spectral data: ' char(setting.path)]);
do_log(folder,textstr);
textstr=(['Nr of spectra: ' num2str(setting.nr)]);
do_log(folder,textstr);
textstr=(['Noise excluded: ' num2str(setting.noise_excl)]);
do_log(folder,textstr);
textstr=(['Aligned spectra: ' char(setting.align)]);
do_log(folder,textstr);
textstr=(['Normalized final spectral region: ' char(setting.normroi)]);
do_log(folder,textstr);
textstr=(['Data scaled to Unit Variance: ' char(setting.uv)]);
do_log(folder,textstr);
textstr='************************************';
do_log(folder,textstr);

for j=1:setting.incl_nr
    S2=size(eval(['setting.K' num2str(j)]),2);
    S=size(eval(['setting.R' num2str(j)]),2);
    setting.dp_final=[setting.dp_final; S2 S S*S2];
    textstr=(['Datapoints after removal of noise (final): ' num2str(eval(['setting.dp_afternoise' num2str(j)]))]);
    do_log(folder,textstr);
    textstr=(['Limits for selected regions (1H_1 1H_2 13C_1 13C_2): ' num2str(setting.incl(j,:))]);
    do_log(folder,textstr);
    textstr=(['Datapoints in regions (F2, F1, total): ' num2str(setting.dp_final(j,:))]);
    do_log(folder,textstr);
    textstr='************************************';
    do_log(folder,textstr);
end

setting.F2ppm=F2_ppm;
setting.F1ppm=F1_ppm;



if isfield(setting,'excl')==0
    textstr=(['Nr of deleted regions: None']);
    do_log(folder,textstr);
else
    a=setting.excl;
    b=size(a,1);
    textstr=(['Nr of deleted regions: ' num2str(size(setting.excl,1))]);
    do_log(folder,textstr);
    for i=1:b
        textstr=(['Limits for deleted regions (1H_1 1H_2 13C_1 13C_2): ' num2str(setting.excl(i,:))]);
        do_log(folder,textstr);
    end
end


if isfield(setting,'merge') == 0
    for i=1:setting.incl_nr
        X=eval(['X2_' num2str(i)]);
        xls_temp=cell(size(X,1),(size(X,2)+1));
        %xls_temp=cell((size(X,1)+1),(size(X,2)+1)); the outcommented lines
        %used for adding the index as first column in the excel sheet.
        xls_temp(1:end,2:end)=num2cell(X);
        %xls_temp(2:end,2:end)=num2cell(X);
        xls_temp(1:end,1)=titles;
        %xls_temp(2:end,1)=titles;
        %add index here in column 1
        %xls_temp(1,2:end)=setting.Index;
        xls=xls_temp';
        file3=['final',num2str(i),'.xls'];
        file2=['final',num2str(i),'.mat'];
        save(fullfile(folder,file2),'X','-v4');
        xlswrite(fullfile(folder,file3),xls);
        
    end
else
    for i=1:size(setting.merge,1)
        X=eval(['X3_' num2str(i)]);
        xls_temp=cell(size(X,1),(size(X,2)+1));
        xls_temp(1:end,2:end)=num2cell(X);
        xls_temp(1:end,1)=titles;
        xls=xls_temp';
        file3=['final',num2str(i),'.xls'];
        file2=['final',num2str(i),'.mat'];
        save(fullfile(folder,file2),'X','-v4');
        xlswrite(fullfile(folder,file3),xls);
      
    end
end
%end
file='settings.mat';
save(fullfile(folder,file),'setting');


% --------------------------------------------------------------------
function file_menu_quit_Callback(hObject, eventdata, handles)

delete 'orig_spect.mat';
delete 'mod_spect.mat';
delete 'current_data.mat';
delete 'PROC_DATA.mat';
close


% --- Executes on selection change in Spectralist.
function Spectralist_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function Spectralist_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Plot plot spectrum.
function Plot_Callback(hObject, eventdata, handles)
load('PROC_DATA.mat');
load('orig_spect.mat');
axes(handles.axes1)
NUM=get(handles.Spectralist,'value');
spektrum=reshape(orig_spect(NUM,:),SI2,SI1);
spektrum=rot90(spektrum);
maxint=max(max(spektrum));
minint=min(min(spektrum));
V=[minint:(maxint-minint)/20:maxint];
if get(handles.Lockbutton,'value')==1
    limits=axis;
    [x_lim,y_lim]=ppm_to_dp(SI2,SI1,F2_ppm,F1_ppm,limits);
    if min(x_lim)<1;
        x_lim=1:SI2;
        y_lim=1:SI1;
    end
    
    contour(F2_ppm(x_lim),F1_ppm(y_lim),spektrum(y_lim,x_lim),V);
    set(gca,'XDir','reverse')
    set(gca,'YDir','reverse')
    xlabel('ppm');
    ylabel('ppm');
    legend(char(titles(NUM)));

else
    contour(F2_ppm,F1_ppm,spektrum,V);
    set(gca,'XDir','reverse')
    set(gca,'YDir','reverse')
    xlabel('ppm');
    ylabel('ppm');
    legend(char(titles(NUM)));
    
end

save('current_data.mat','SI1','SI2','spektrum','V','F2_ppm','F1_ppm','NUM','titles');


% --- Executes on button press in increasebutton. Higher Intensity.
function increasebutton_Callback(hObject, eventdata, handles)
axes(handles.axes1);
load current_data.mat
spektrum=spektrum*2;
if get(handles.Lockbutton,'value')==1
    limits=axis;
    [x_lim,y_lim]=ppm_to_dp(SI2,SI1,F2_ppm,F1_ppm,limits);
    if min(x_lim)<1;
        x_lim=1:SI2;
        y_lim=1:SI1;
    end
    contour(F2_ppm(x_lim),F1_ppm(y_lim),spektrum(y_lim,x_lim),V);
else
    contour(F2_ppm,F1_ppm,spektrum,V);
end
set(gca,'XDir','reverse')
set(gca,'YDir','reverse')
xlabel('ppm');
ylabel('ppm');
legend(char(titles(NUM)));

save('current_data.mat','SI1','SI2','spektrum','V','F2_ppm','F1_ppm','NUM','titles');

% --- Executes on button press in decreasebutton. Lower intensity.
function decreasebutton_Callback(hObject, eventdata, handles)
axes(handles.axes1);
load current_data.mat
spektrum=spektrum/2;
if get(handles.Lockbutton,'value')==1
    limits=axis;
    [x_lim,y_lim]=ppm_to_dp(SI2,SI1,F2_ppm,F1_ppm,limits);
    if min(x_lim)<1;
        x_lim=1:SI2;
        y_lim=1:SI1;
    end
    contour(F2_ppm(x_lim),F1_ppm(y_lim),spektrum(y_lim,x_lim),V);
else
    contour(F2_ppm,F1_ppm,spektrum,V);
end
set(gca,'XDir','reverse')
set(gca,'YDir','reverse')
xlabel('ppm');
ylabel('ppm');
legend(char(titles(NUM)));

save('current_data.mat','SI1','SI2','spektrum','V','F2_ppm','F1_ppm','NUM','titles');


% --- Executes on button press in All.
function All_Callback(hObject, eventdata, handles)
axes(handles.axes1);
load current_data.mat
contour(F2_ppm,F1_ppm,spektrum,V);
set(gca,'XDir','reverse')
set(gca,'YDir','reverse')
xlabel('ppm');
ylabel('ppm');
legend(char(titles(NUM)));

save('current_data.mat','SI1','SI2','spektrum','V','F2_ppm','F1_ppm','NUM','titles');


% --- Executes on button press in IncludeROI. Include region
function IncludeROI_Callback(hObject, eventdata, handles)

set(handles.infotext,'string','Press any key to end selection!');
zoom off
if exist('settings.mat') == 2
    load settings.mat;
    if isfield(setting,'incl') == 1;
        incl_roi_ppm = setting.incl;
    else
        incl_roi_ppm = [];
    end
else
    incl_roi_ppm=[];
end
k=0;
while k == 0
    k = waitforbuttonpress;
    point1 = get(gca,'CurrentPoint');    % button down detected
    finalRect = rbbox;                   % return figure units
    point2 = get(gca,'CurrentPoint');    % button up detected
    point1 = point1(1,1:2);              % extract x and y
    point2 = point2(1,1:2);
    p1 = min(point1,point2);             % calculate locations
    offset = abs(point1-point2);         % and dimensions
    x = [p1(1) p1(1)+offset(1) p1(1)+offset(1) p1(1) p1(1)];
    y = [p1(2) p1(2) p1(2)+offset(2) p1(2)+offset(2) p1(2)];
    hold on
    plot(x,y,'g','linewidth',1)          % draw box around selected region
    incl_roi_ppm=[incl_roi_ppm; x(1) x(2) y(1) y(3)];
end
incl_roi_ppm=incl_roi_ppm(1:end-1,:);
setting.incl = incl_roi_ppm ;
hold off
set(handles.infotext,'string','');
save settings.mat setting;


% --- Executes on button press in Cursorbutton.
function Cursorbutton_Callback(hObject, eventdata, handles)

datacursormode
set(handles.Zoombutton,'value',0);


% --- Executes on button press in Removepeak. Remove peak
function Removepeak_Callback(hObject, eventdata, handles)

set(handles.infotext,'string','Press any key to end selection!');
zoom off
if exist('settings.mat') == 2
    load settings.mat;
    if isfield(setting,'excl') == 1;
        excl_roi_ppm = setting.excl;
    else
        excl_roi_ppm = [];
    end
else
    excl_roi_ppm=[];
end
k=0;
while k == 0
    k = waitforbuttonpress;
    point1 = get(gca,'CurrentPoint');    % button down detected
    finalRect = rbbox;                   % return figure units
    point2 = get(gca,'CurrentPoint');    % button up detected
    point1 = point1(1,1:2);              % extract x and y
    point2 = point2(1,1:2);
    p1 = min(point1,point2);             % calculate locations
    offset = abs(point1-point2);         % and dimensions
    x = [p1(1) p1(1)+offset(1) p1(1)+offset(1) p1(1) p1(1)];
    y = [p1(2) p1(2) p1(2)+offset(2) p1(2)+offset(2) p1(2)];
    hold on
    plot(x,y,'r','linewidth',1)          % draw box around selected region
    excl_roi_ppm=[excl_roi_ppm; x(1) x(2) y(1) y(3)];
end
excl_roi_ppm=excl_roi_ppm(1:end-1,:);
setting.excl = excl_roi_ppm ;
hold off
set(handles.infotext,'string','');
save settings.mat setting;


% --- Executes on button press in Zoombutton.
function Zoombutton_Callback(hObject, eventdata, handles)

zoom on
set(handles.Cursorbutton,'value',0);


% --- Executes on button press in Align. Align spectra
function Align_Callback(hObject, eventdata, handles)
if exist('settings.mat') == 2;
    load settings.mat
end
load('PROC_DATA.mat')
load('orig_spect.mat')
set(handles.infotext,'string','Zoom in on peak to align then press enter.');
pause
limits=axis;
[x_lim,y_lim]=ppm_to_dp(SI2,SI1,F2_ppm,F1_ppm,limits);
ref_spec=get(handles.Spectralist,'value');
set(handles.infotext,'string','Aligning spectra!');
pause(0.2);
[orig_spect]=NMR_align2D(orig_spect,ref_spec,SI1,SI2,F2_ppm,F1_ppm,x_lim,y_lim);
set(handles.infotext,'string','');
save('orig_spect.mat','orig_spect');
setting.align='YES';
save('settings.mat','setting');


% --- Executes on button press in Restart.
function Restart_Callback(hObject, eventdata, handles)

load settings.mat;
if isfield(setting,'incl') == 1;
    setting = rmfield(setting,'incl');
end
if isfield(setting,'excl') == 1;
    setting = rmfield(setting,'excl');
end
if isfield(setting,'first') == 1;
    setting = rmfield(setting,'first');
end
if isfield(setting,'merge') == 1;
    setting = rmfield(setting,'merge');
end
save settings.mat,'setting';



% --- Executes on button press in Noise. Noise exclusion
function Noise_Callback(hObject, eventdata, handles)

load('orig_spect.mat');
if exist('settings.mat') == 2;
    load settings.mat
end
fig=figure;
plot(orig_spect(1,:));
set(handles.infotext,'string','Mark noise threshold');
pause
dcm_obj = datacursormode(fig);
info_struct = getCursorInfo(dcm_obj);
pos = info_struct.Position;
noise=pos(1,2);
close
setting.noise = noise;
set(handles.infotext,'string','');
save settings.mat setting;

% --- Executes on button press in Process. Process
function Process_Callback(hObject, eventdata, handles)

load('PROC_DATA.mat');
load('orig_spect.mat');
if exist('settings.mat')==2;
load('settings.mat');
end
mod_spect=[];
mod_spect2=[];
X2_single=[];
X2=[];
nr = size(orig_spect,1);
if isfield(setting,'excl') == 1;
   
    excl_roi_ppm = setting.excl ;
    set(handles.infotext,'string','Please wait');
    pause(0.1);
    for i = 1 : nr;
        DATA=reshape(orig_spect(i,:),SI2,SI1);% vector to matrix, whole spectrum
        DATA=rot90(DATA);%rotate spectrum to get axes right
        for j = 1 : size(excl_roi_ppm,1);
            limits = excl_roi_ppm(j,:);
            [x_lim,y_lim]=ppm_to_dp(SI2,SI1,F2_ppm,F1_ppm,limits);
            DATA(y_lim,x_lim) = 0; %Set intensities in excluded data points to zero
        end
        DATA=rot90(DATA,3);
        Vector=reshape(DATA,1,SI2*SI1);%Put data in vector
        mod_spect=[mod_spect;Vector];%Put all vectors in matrix mod_spect
    end
end
if isfield(setting,'incl') == 1;
   
    incl_roi_ppm = setting.incl;
    if size(mod_spect,1)==0;
        spectra=orig_spect;
    else
        spectra=mod_spect;
    end
    setting.incl_nr=size(incl_roi_ppm,1);

    %loop through the included regions and extract ROI sizes and limits
    for i=1:size(incl_roi_ppm,1);
        limits=incl_roi_ppm(i,:);
        [x_lim,y_lim]=ppm_to_dp(SI2,SI1,F2_ppm,F1_ppm,limits);
        K=x_lim;
        R=y_lim;
        eval(['setting.R' num2str(i) '=R']);
        eval(['setting.K' num2str(i) '=K']);
        eval(['setting.S' num2str(i) '=size(R,2)']);
        eval(['setting.S2' num2str(i) '=size(K,2)']);
        eval(['F2_ppm_roi' num2str(i) '=F2_ppm(x_lim)']);
        eval(['F1_ppm_roi' num2str(i) '=F1_ppm(y_lim)']);
    end

    %loop through the included regions and place them as vectors in X2
    for i=1:size(incl_roi_ppm,1);
        R=eval(['setting.R' num2str(i)]);
        K=eval(['setting.K' num2str(i)]);
        [X2,S,S2]=ROI_to_vector(spectra,SI2,SI1,nr,R,K);

        % Optional noise exclusion
        if get(handles.excl_noise,'value')==1;
            if isfield(setting,'noise') == 0;
                warndlg('Noise threshold not defined!');
            else
                noise = setting.noise;

                setting.noise_excl='YES';
                [final,Index]=excl_noise(X2,noise);
                X2 = final;
                eval(['setting.Index' num2str(i) '=Index']);
                eval(['setting.dp_afternoise' num2str(i) '=size(final)']);
            end
        else
            setting.noise_excl='NO';
        end

        % Optional normalization of ROI
        if isfield(setting,'merge') == 0 %separate norm. for merged regions
            if get(handles.NormROIradio,'value')==1;
                setting.normroi='YES';
                for j=1:size(X2,1);
                    datasum = sum(X2(j,:));
                    scale = 1000000/datasum;
                    X2(j,:)=X2(j,:)*scale;
                end
            else
                setting.normroi='NO';
            end
        end

        % Calculate standard deviations of variables
        std_dev = std(X2);
        eval(['setting.std' num2str(i) '=std_dev']);
        
        % Optional UV-scaling (and centering)
        if isfield(setting,'merge') == 0 %separate scaling for merged region
            if get(handles.UVradio,'value')==1;
                setting.uv='Yes';
                
                X2_mean=zeros(size(X2,1),size(X2,2));
                X2_UV=zeros(size(X2,1),size(X2,2));
                
                
                for j=1:size(X2,2);
                    X2_UV(:,j)=X2(:,j)/std_dev(j);
                end
                average=mean(X2_UV);
                for j=1:size(X2,2);
                    X2_mean(:,j) = X2_UV(:,j)-average(j);
                end
                
                X2=X2_mean;
              
            else
                setting.uv='no';
            end
        end


        % Place ROIs in different matrices
        eval(['X2_' num2str(i) '=X2']);
    end
end


%Merge regions and save
if isfield(setting,'merge') == 1
    X2_merge=[];
    for i = 1 : size(setting.merge,1)
        a = setting.merge(i,1);
        b = setting.merge(i,2);
        for j = a : b
            X2_temp = eval(['X2_' num2str(j)]);
            X2_merge = [X2_merge X2_temp];
        end


        % Optional normalization of merged ROIs
        if get(handles.NormROIradio,'value')==1;
            setting.normroi='YES';
            for j=1:size(X2_merge,1);
                datasum = sum(X2_merge(j,:));
                scale = 1000000/datasum;
                X2_merge(j,:)=X2_merge(j,:)*scale;
            end
        else
            setting.normroi='NO';
        end

        % Optional UV-scaling of merged ROIs
        if get(handles.UVradio,'value')==1;
            setting.uv='Yes';
            std_dev=std(X2_merge);
            eval(['setting.std' num2str(i) '=std_dev']);
            for j=1:size(X2_merge,1);
                X2_merge(j,:)./std_dev;
            end
        else
            setting.uv='no';
        end
        eval(['X3_' num2str(i) '=X2_merge']);
    end

    save mod_spect X3*;
else
    save mod_spect.mat X2*;
end

save('settings.mat','setting');
set(handles.infotext,'string','');




% --- Executes on button press in ShowROI. Show regions
function ShowROI_Callback(hObject, eventdata, handles)

load settings.mat
if isfield(setting,'excl') == 1
    excl_roi_ppm = setting.excl;
    hold on
    for i=1:size(excl_roi_ppm,1);
        lim=excl_roi_ppm(i,:);
        p=patch([lim(1,2) lim(1,2) lim(1,1) lim(1,1)],[lim(1,3) lim(1,4) lim(1,4) lim(1,3)],'r');
    end
    set(p,'facealpha',.5);
    set(p,'linestyle','none');
end
if isfield(setting,'incl') == 1
    incl_roi_ppm = setting.incl;
    for i=1:size(incl_roi_ppm,1);
        lim=incl_roi_ppm(i,:);
        p1=patch([lim(1,2) lim(1,2) lim(1,1) lim(1,1)],[lim(1,3) lim(1,4) lim(1,4) lim(1,3)],'g');
    end
    set(p1,'facealpha',.5);
    set(p1,'linestyle','none');
end

hold off



% --- Executes on button press in NormROIradio.
function NormROIradio_Callback(hObject, eventdata, handles)


% --- Executes on button press in Lockbutton.
function Lockbutton_Callback(hObject, eventdata, handles)


% --- Executes on button press in excl_noise.
function excl_noise_Callback(hObject, eventdata, handles)


% --- Executes on button press in Loadings.
function Loadings_Callback(hObject, eventdata, handles)

NMR_vis



% --- Executes on button press in UVradio.
function UVradio_Callback(hObject, eventdata, handles)


% --- Executes on button press in merge.
function merge_Callback(hObject, eventdata, handles)

if exist('settings.mat')==2
    load settings.mat
    if isfield(setting,'first') == 0 && isfield(setting,'merge') == 0
        setting.first = 1;
        setting.merge = [];
    end
else
    setting.first=1;
    setting.merge=[];
end
incl_roi_ppm = setting.incl;
first=setting.first;
last = size(incl_roi_ppm,1);
setting.merge=[setting.merge;first last];
for i = first:last
    lim=incl_roi_ppm(i,:);
    p=patch([lim(1,2) lim(1,2) lim(1,1) lim(1,1)],[lim(1,3) lim(1,4) lim(1,4) lim(1,3)],'g');
    text(lim(1,2),lim(1,3),num2str(size(setting.merge,1)));
    set(p,'facealpha',.5);
    set(p,'linestyle','none');
end
setting.first=last+1;
save('settings.mat','setting');


% --------------------------------------------------------------------
function about_menu_Callback(hObject, eventdata, handles)
fid = fopen('About_NMR_proc.txt','r');
str = fread(fid);
a = find(str==13);
str(a)=[];
fclose(fid);
uiwait(msgbox(char(str'),'About','modal'));



