function do_log(pathname,textstr);

fid=fopen(fullfile(pathname,'logfile.txt'),'a+');
tline = fgetl(fid);
while length(tline)>1
    tline = fgetl(fid);
end
infostr=['%' num2str(length(textstr)) '.' num2str(length(textstr)) 's\n'];
fprintf(fid,infostr,textstr);

fclose(fid);

