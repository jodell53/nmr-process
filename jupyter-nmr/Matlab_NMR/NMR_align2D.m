% Copyright (c) Mattias Hedenström, 2008, 2009, 2010 (mattias.hedenstrom@chem.umu.se)
%
%    This file is part of the NMR_proc GUI
%    NMR_proc is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    This program is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%    Version 1.0 Release data: 2010-05-17
function [orig_spect]=NMR_align2D(orig_spect,ref_spec,SI1,SI2,F1_ppm,F2_ppm,x_lim,y_lim);

orig_spect2=[];
spektrum=reshape(orig_spect(ref_spec,:),SI2,SI1);
spektrum=rot90(spektrum);

zero_spect=spektrum(y_lim,x_lim);
max_x_ref=find(max(zero_spect)==max(max(zero_spect)));
max_y_ref=find(max(zero_spect')==max(max(zero_spect')));
for i=1:size(orig_spect,1);
    spect=reshape(orig_spect(i,:),SI2,SI1);
    spect=rot90(spect);
    zero_spect=spect(y_lim,x_lim);
    max_x=find(max(zero_spect)==max(max(zero_spect)));
    max_y=find(max(zero_spect')==max(max(zero_spect')));
    shift_x=max_x_ref-max_x;
    shift_y=max_y_ref-max_y;
    spect_align=zeros(SI1,SI2);
    if shift_y>0;
        if shift_x>0;
            spect_align((1+shift_y):SI1,(1+shift_x):SI2)=spect(1:(SI1-shift_y),1:(SI2-shift_x));
        else
            spect_align((1+shift_y):SI1,1:(SI2+shift_x))=spect(1:(SI1-shift_y),(1-shift_x):SI2);
        end
    else
        if shift_x>0;
            spect_align(1:(SI1+shift_y),(1+shift_x):SI2)=spect((1-shift_y):SI1,1:(SI2-shift_x));
        else
            spect_align(1:(SI1+shift_y),1:(SI2+shift_x))=spect((1-shift_y):SI1,(1-shift_x):SI2);
        end
    end
    spect_align=rot90(spect_align,3);
    Vector=reshape(spect_align,1,SI1*SI2);
    orig_spect2=[orig_spect2;Vector];
end
orig_spect=orig_spect2;