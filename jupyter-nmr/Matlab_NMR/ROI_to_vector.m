% Copyright (c) Mattias Hedenström 2008,2009,2010 (mattias.hedenstrom@chem.umu.se)
% 
%    This file is part of the NMR_proc GUI
%    NMR_proc is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the Free Software Foundation, either version 3 of the License, or
%    (at your option) any later version.
%
%    This program is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%    GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%    Version 1.0 Release data: 2010-05-17
function [X2,S,S2]=ROI_to_vector_new(spectra,SI2,SI1,nr,R,K)

X2=[];
S=[];
S2=[];

for i=1:nr
    DATA=reshape(spectra(i,:),SI2,SI1); %vector to matrix, whole spectrum
    DATA=rot90(DATA);                   %rotate
    DATA_NEW=DATA(R,K);                 %R=rows K=columns, defined by region selection
    S2=size(DATA_NEW,2);                %nr of columns in the new matrix
    S=size(DATA_NEW,1);                 %nr of rows in the new matrix
    Vector=reshape(DATA_NEW,1,S*S2);    %put ROI in vector
    
    X2=[X2;Vector];                     %gather all vector in a matrix
end