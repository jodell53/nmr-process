#Dockerfile includes jupyter-shows nmrglue v0.7 installed
#docker build -t condabuild .
#rerun with jupyter commented out and `conda build --no-cache=true...` gave same version of nmrglue
Sending build context to Docker daemon  5.997MB
Step 1/7 : FROM continuumio/miniconda3
 ---> 52daacd3dd5d
Step 2/7 : RUN conda update -n base -c defaults conda
 ---> Using cache
 ---> 71e4837848a4
Step 3/7 : RUN conda install python=3.6
 ---> Using cache
 ---> 8fd21b025028
Step 4/7 : RUN conda install numpy scipy matplotlib ipython jupyter
 ---> Running in 90b95cd94171
Collecting package metadata (current_repodata.json): ...working... done
Solving environment: ...working... failed with initial frozen solve. Retrying with flexible solve.
Solving environment: ...working... failed with repodata from current_repodata.json, will retry with next repodata source.
Collecting package metadata (repodata.json): ...working... done
Solving environment: ...working... 
Warning: 2 possible package resolutions (only showing differing packages):
  - defaults/linux-64::jedi-0.17.0-py36_0, defaults/noarch::parso-0.8.1-pyhd3eb1b0_0
  - defaults/linux-64::jedi-0.17.2-py36h06a4308_1, defaults/noarch::parso-0.7.0-py_0done

## Package Plan ##

  environment location: /opt/conda

  added / updated specs:
    - ipython
    - jupyter
    - matplotlib
    - numpy
    - scipy


The following packages will be downloaded:

    package                    |            build
    ---------------------------|-----------------
    argon2-cffi-20.1.0         |   py36h7b6447c_1          46 KB
    async_generator-1.10       |   py36h28b3542_0          39 KB
    attrs-20.3.0               |     pyhd3eb1b0_0          43 KB
    backcall-0.2.0             |     pyhd3eb1b0_0          13 KB
    blas-1.0                   |              mkl           6 KB
    bleach-3.2.3               |     pyhd3eb1b0_0         113 KB
    ca-certificates-2021.1.19  |       h06a4308_0         121 KB
    cycler-0.10.0              |           py36_0          13 KB
    dbus-1.13.18               |       hb2f20db_0         504 KB
    decorator-4.4.2            |     pyhd3eb1b0_0          12 KB
    defusedxml-0.6.0           |             py_0          23 KB
    entrypoints-0.3            |           py36_0          12 KB
    expat-2.2.10               |       he6710b0_2         153 KB
    fontconfig-2.13.0          |       h9420a91_0         227 KB
    freetype-2.10.4            |       h5ab3b9f_0         596 KB
    glib-2.66.1                |       h92f7085_0         2.9 MB
    gst-plugins-base-1.14.0    |       h8213a91_2         4.9 MB
    gstreamer-1.14.0           |       h28cd5cc_2         3.2 MB
    icu-58.2                   |       he6710b0_3        10.5 MB
    importlib-metadata-2.0.0   |             py_1          35 KB
    importlib_metadata-2.0.0   |                1          11 KB
    intel-openmp-2020.2        |              254         786 KB
    ipykernel-5.3.4            |   py36h5ca1d4c_0         181 KB
    ipython-7.16.1             |   py36h5ca1d4c_0         999 KB
    ipython_genutils-0.2.0     |     pyhd3eb1b0_1          27 KB
    ipywidgets-7.6.3           |     pyhd3eb1b0_1         105 KB
    jedi-0.17.0                |           py36_0         780 KB
    jinja2-2.11.2              |     pyhd3eb1b0_0          93 KB
    jpeg-9b                    |       h024ee3a_2         214 KB
    jsonschema-3.2.0           |             py_2          47 KB
    jupyter-1.0.0              |           py36_7           6 KB
    jupyter_client-6.1.7       |             py_0          77 KB
    jupyter_console-6.2.0      |             py_0          26 KB
    jupyter_core-4.7.0         |   py36h06a4308_0          68 KB
    jupyterlab_pygments-0.1.2  |             py_0           8 KB
    jupyterlab_widgets-1.0.0   |     pyhd3eb1b0_1         109 KB
    kiwisolver-1.3.0           |   py36h2531618_0          80 KB
    lcms2-2.11                 |       h396b838_0         307 KB
    libgfortran-ng-7.3.0       |       hdf63c60_0        1006 KB
    libpng-1.6.37              |       hbc83047_0         278 KB
    libsodium-1.0.18           |       h7b6447c_0         244 KB
    libtiff-4.1.0              |       h2733197_1         449 KB
    libuuid-1.0.3              |       h1bed415_2          15 KB
    libxcb-1.14                |       h7b6447c_0         505 KB
    libxml2-2.9.10             |       hb55368b_3         1.2 MB
    lz4-c-1.9.3                |       h2531618_0         186 KB
    markupsafe-1.1.1           |   py36h7b6447c_0          29 KB
    matplotlib-3.3.2           |       h06a4308_0          24 KB
    matplotlib-base-3.3.2      |   py36h817c723_0         5.1 MB
    mistune-0.8.4              |   py36h7b6447c_0          55 KB
    mkl-2020.2                 |              256       138.3 MB
    mkl-service-2.3.0          |   py36he8ac12f_0          52 KB
    mkl_fft-1.2.0              |   py36h23d657b_0         149 KB
    mkl_random-1.1.1           |   py36h0573a6f_0         327 KB
    nbclient-0.5.1             |             py_0          58 KB
    nbconvert-6.0.7            |           py36_0         480 KB
    nbformat-5.1.2             |     pyhd3eb1b0_1          68 KB
    nest-asyncio-1.4.3         |     pyhd3eb1b0_0          11 KB
    notebook-6.2.0             |   py36h06a4308_0         4.1 MB
    numpy-1.19.2               |   py36h54aff64_0          22 KB
    numpy-base-1.19.2          |   py36hfa32c7d_0         4.1 MB
    olefile-0.46               |           py36_0          48 KB
    packaging-20.9             |     pyhd3eb1b0_0          37 KB
    pandoc-2.11                |       hb0f4dca_0         9.6 MB
    pandocfilters-1.4.3        |   py36h06a4308_1          14 KB
    parso-0.8.1                |     pyhd3eb1b0_0          69 KB
    pcre-8.44                  |       he6710b0_0         212 KB
    pexpect-4.8.0              |     pyhd3eb1b0_3          53 KB
    pickleshare-0.7.5          |  pyhd3eb1b0_1003          13 KB
    pillow-8.1.0               |   py36he98fc37_0         626 KB
    prometheus_client-0.9.0    |     pyhd3eb1b0_0          45 KB
    prompt-toolkit-3.0.8       |             py_0         248 KB
    prompt_toolkit-3.0.8       |                0          12 KB
    ptyprocess-0.7.0           |     pyhd3eb1b0_2          17 KB
    pygments-2.7.4             |     pyhd3eb1b0_0         676 KB
    pyparsing-2.4.7            |     pyhd3eb1b0_0          59 KB
    pyqt-5.9.2                 |   py36h05f1152_2         4.5 MB
    pyrsistent-0.17.3          |   py36h7b6447c_0          89 KB
    python-dateutil-2.8.1      |     pyhd3eb1b0_0         221 KB
    pyzmq-20.0.0               |   py36h2531618_1         438 KB
    qt-5.9.7                   |       h5867ecd_1        68.5 MB
    qtconsole-4.7.7            |             py_0          96 KB
    qtpy-1.9.0                 |             py_0          38 KB
    scipy-1.5.2                |   py36h0b6359f_0        14.4 MB
    send2trash-1.5.0           |     pyhd3eb1b0_1          14 KB
    sip-4.19.8                 |   py36hf484d3e_0         274 KB
    terminado-0.9.2            |   py36h06a4308_0          25 KB
    testpath-0.4.4             |     pyhd3eb1b0_0          85 KB
    tornado-6.1                |   py36h27cfd23_0         581 KB
    traitlets-4.3.3            |           py36_0         140 KB
    wcwidth-0.2.5              |             py_0          29 KB
    webencodings-0.5.1         |           py36_1          19 KB
    widgetsnbextension-3.5.1   |           py36_0         862 KB
    zeromq-4.3.3               |       he6710b0_3         500 KB
    zipp-3.4.0                 |     pyhd3eb1b0_0          15 KB
    zstd-1.4.5                 |       h9ceee32_0         619 KB
    ------------------------------------------------------------
                                           Total:       287.0 MB

The following NEW packages will be INSTALLED:

  argon2-cffi        pkgs/main/linux-64::argon2-cffi-20.1.0-py36h7b6447c_1
  async_generator    pkgs/main/linux-64::async_generator-1.10-py36h28b3542_0
  attrs              pkgs/main/noarch::attrs-20.3.0-pyhd3eb1b0_0
  backcall           pkgs/main/noarch::backcall-0.2.0-pyhd3eb1b0_0
  blas               pkgs/main/linux-64::blas-1.0-mkl
  bleach             pkgs/main/noarch::bleach-3.2.3-pyhd3eb1b0_0
  cycler             pkgs/main/linux-64::cycler-0.10.0-py36_0
  dbus               pkgs/main/linux-64::dbus-1.13.18-hb2f20db_0
  decorator          pkgs/main/noarch::decorator-4.4.2-pyhd3eb1b0_0
  defusedxml         pkgs/main/noarch::defusedxml-0.6.0-py_0
  entrypoints        pkgs/main/linux-64::entrypoints-0.3-py36_0
  expat              pkgs/main/linux-64::expat-2.2.10-he6710b0_2
  fontconfig         pkgs/main/linux-64::fontconfig-2.13.0-h9420a91_0
  freetype           pkgs/main/linux-64::freetype-2.10.4-h5ab3b9f_0
  glib               pkgs/main/linux-64::glib-2.66.1-h92f7085_0
  gst-plugins-base   pkgs/main/linux-64::gst-plugins-base-1.14.0-h8213a91_2
  gstreamer          pkgs/main/linux-64::gstreamer-1.14.0-h28cd5cc_2
  icu                pkgs/main/linux-64::icu-58.2-he6710b0_3
  importlib-metadata pkgs/main/noarch::importlib-metadata-2.0.0-py_1
  importlib_metadata pkgs/main/noarch::importlib_metadata-2.0.0-1
  intel-openmp       pkgs/main/linux-64::intel-openmp-2020.2-254
  ipykernel          pkgs/main/linux-64::ipykernel-5.3.4-py36h5ca1d4c_0
  ipython            pkgs/main/linux-64::ipython-7.16.1-py36h5ca1d4c_0
  ipython_genutils   pkgs/main/noarch::ipython_genutils-0.2.0-pyhd3eb1b0_1
  ipywidgets         pkgs/main/noarch::ipywidgets-7.6.3-pyhd3eb1b0_1
  jedi               pkgs/main/linux-64::jedi-0.17.0-py36_0
  jinja2             pkgs/main/noarch::jinja2-2.11.2-pyhd3eb1b0_0
  jpeg               pkgs/main/linux-64::jpeg-9b-h024ee3a_2
  jsonschema         pkgs/main/noarch::jsonschema-3.2.0-py_2
  jupyter            pkgs/main/linux-64::jupyter-1.0.0-py36_7
  jupyter_client     pkgs/main/noarch::jupyter_client-6.1.7-py_0
  jupyter_console    pkgs/main/noarch::jupyter_console-6.2.0-py_0
  jupyter_core       pkgs/main/linux-64::jupyter_core-4.7.0-py36h06a4308_0
  jupyterlab_pygmen~ pkgs/main/noarch::jupyterlab_pygments-0.1.2-py_0
  jupyterlab_widgets pkgs/main/noarch::jupyterlab_widgets-1.0.0-pyhd3eb1b0_1
  kiwisolver         pkgs/main/linux-64::kiwisolver-1.3.0-py36h2531618_0
  lcms2              pkgs/main/linux-64::lcms2-2.11-h396b838_0
  libgfortran-ng     pkgs/main/linux-64::libgfortran-ng-7.3.0-hdf63c60_0
  libpng             pkgs/main/linux-64::libpng-1.6.37-hbc83047_0
  libsodium          pkgs/main/linux-64::libsodium-1.0.18-h7b6447c_0
  libtiff            pkgs/main/linux-64::libtiff-4.1.0-h2733197_1
  libuuid            pkgs/main/linux-64::libuuid-1.0.3-h1bed415_2
  libxcb             pkgs/main/linux-64::libxcb-1.14-h7b6447c_0
  libxml2            pkgs/main/linux-64::libxml2-2.9.10-hb55368b_3
  lz4-c              pkgs/main/linux-64::lz4-c-1.9.3-h2531618_0
  markupsafe         pkgs/main/linux-64::markupsafe-1.1.1-py36h7b6447c_0
  matplotlib         pkgs/main/linux-64::matplotlib-3.3.2-h06a4308_0
  matplotlib-base    pkgs/main/linux-64::matplotlib-base-3.3.2-py36h817c723_0
  mistune            pkgs/main/linux-64::mistune-0.8.4-py36h7b6447c_0
  mkl                pkgs/main/linux-64::mkl-2020.2-256
  mkl-service        pkgs/main/linux-64::mkl-service-2.3.0-py36he8ac12f_0
  mkl_fft            pkgs/main/linux-64::mkl_fft-1.2.0-py36h23d657b_0
  mkl_random         pkgs/main/linux-64::mkl_random-1.1.1-py36h0573a6f_0
  nbclient           pkgs/main/noarch::nbclient-0.5.1-py_0
  nbconvert          pkgs/main/linux-64::nbconvert-6.0.7-py36_0
  nbformat           pkgs/main/noarch::nbformat-5.1.2-pyhd3eb1b0_1
  nest-asyncio       pkgs/main/noarch::nest-asyncio-1.4.3-pyhd3eb1b0_0
  notebook           pkgs/main/linux-64::notebook-6.2.0-py36h06a4308_0
  numpy              pkgs/main/linux-64::numpy-1.19.2-py36h54aff64_0
  numpy-base         pkgs/main/linux-64::numpy-base-1.19.2-py36hfa32c7d_0
  olefile            pkgs/main/linux-64::olefile-0.46-py36_0
  packaging          pkgs/main/noarch::packaging-20.9-pyhd3eb1b0_0
  pandoc             pkgs/main/linux-64::pandoc-2.11-hb0f4dca_0
  pandocfilters      pkgs/main/linux-64::pandocfilters-1.4.3-py36h06a4308_1
  parso              pkgs/main/noarch::parso-0.8.1-pyhd3eb1b0_0
  pcre               pkgs/main/linux-64::pcre-8.44-he6710b0_0
  pexpect            pkgs/main/noarch::pexpect-4.8.0-pyhd3eb1b0_3
  pickleshare        pkgs/main/noarch::pickleshare-0.7.5-pyhd3eb1b0_1003
  pillow             pkgs/main/linux-64::pillow-8.1.0-py36he98fc37_0
  prometheus_client  pkgs/main/noarch::prometheus_client-0.9.0-pyhd3eb1b0_0
  prompt-toolkit     pkgs/main/noarch::prompt-toolkit-3.0.8-py_0
  prompt_toolkit     pkgs/main/noarch::prompt_toolkit-3.0.8-0
  ptyprocess         pkgs/main/noarch::ptyprocess-0.7.0-pyhd3eb1b0_2
  pygments           pkgs/main/noarch::pygments-2.7.4-pyhd3eb1b0_0
  pyparsing          pkgs/main/noarch::pyparsing-2.4.7-pyhd3eb1b0_0
  pyqt               pkgs/main/linux-64::pyqt-5.9.2-py36h05f1152_2
  pyrsistent         pkgs/main/linux-64::pyrsistent-0.17.3-py36h7b6447c_0
  python-dateutil    pkgs/main/noarch::python-dateutil-2.8.1-pyhd3eb1b0_0
  pyzmq              pkgs/main/linux-64::pyzmq-20.0.0-py36h2531618_1
  qt                 pkgs/main/linux-64::qt-5.9.7-h5867ecd_1
  qtconsole          pkgs/main/noarch::qtconsole-4.7.7-py_0
  qtpy               pkgs/main/noarch::qtpy-1.9.0-py_0
  scipy              pkgs/main/linux-64::scipy-1.5.2-py36h0b6359f_0
  send2trash         pkgs/main/noarch::send2trash-1.5.0-pyhd3eb1b0_1
  sip                pkgs/main/linux-64::sip-4.19.8-py36hf484d3e_0
  terminado          pkgs/main/linux-64::terminado-0.9.2-py36h06a4308_0
  testpath           pkgs/main/noarch::testpath-0.4.4-pyhd3eb1b0_0
  tornado            pkgs/main/linux-64::tornado-6.1-py36h27cfd23_0
  traitlets          pkgs/main/linux-64::traitlets-4.3.3-py36_0
  wcwidth            pkgs/main/noarch::wcwidth-0.2.5-py_0
  webencodings       pkgs/main/linux-64::webencodings-0.5.1-py36_1
  widgetsnbextension pkgs/main/linux-64::widgetsnbextension-3.5.1-py36_0
  zeromq             pkgs/main/linux-64::zeromq-4.3.3-he6710b0_3
  zipp               pkgs/main/noarch::zipp-3.4.0-pyhd3eb1b0_0
  zstd               pkgs/main/linux-64::zstd-1.4.5-h9ceee32_0

The following packages will be UPDATED:

  ca-certificates                      2020.12.8-h06a4308_0 --> 2021.1.19-h06a4308_0


Proceed ([y]/n)? 

Downloading and Extracting Packages
fontconfig-2.13.0    | 227 KB    | ########## | 100% 
freetype-2.10.4      | 596 KB    | ########## | 100% 
numpy-base-1.19.2    | 4.1 MB    | ########## | 100% 
libtiff-4.1.0        | 449 KB    | ########## | 100% 
matplotlib-3.3.2     | 24 KB     | ########## | 100% 
terminado-0.9.2      | 25 KB     | ########## | 100% 
cycler-0.10.0        | 13 KB     | ########## | 100% 
glib-2.66.1          | 2.9 MB    | ########## | 100% 
numpy-1.19.2         | 22 KB     | ########## | 100% 
jupyterlab_pygments- | 8 KB      | ########## | 100% 
libxcb-1.14          | 505 KB    | ########## | 100% 
olefile-0.46         | 48 KB     | ########## | 100% 
mistune-0.8.4        | 55 KB     | ########## | 100% 
pcre-8.44            | 212 KB    | ########## | 100% 
attrs-20.3.0         | 43 KB     | ########## | 100% 
sip-4.19.8           | 274 KB    | ########## | 100% 
jpeg-9b              | 214 KB    | ########## | 100% 
ipython-7.16.1       | 999 KB    | ########## | 100% 
pandocfilters-1.4.3  | 14 KB     | ########## | 100% 
pyrsistent-0.17.3    | 89 KB     | ########## | 100% 
widgetsnbextension-3 | 862 KB    | ########## | 100% 
pandoc-2.11          | 9.6 MB    | ########## | 100% 
pyzmq-20.0.0         | 438 KB    | ########## | 100% 
zeromq-4.3.3         | 500 KB    | ########## | 100% 
ipywidgets-7.6.3     | 105 KB    | ########## | 100% 
pygments-2.7.4       | 676 KB    | ########## | 100% 
ipython_genutils-0.2 | 27 KB     | ########## | 100% 
gst-plugins-base-1.1 | 4.9 MB    | ########## | 100% 
python-dateutil-2.8. | 221 KB    | ########## | 100% 
libgfortran-ng-7.3.0 | 1006 KB   | ########## | 100% 
libuuid-1.0.3        | 15 KB     | ########## | 100% 
prompt_toolkit-3.0.8 | 12 KB     | ########## | 100% 
prometheus_client-0. | 45 KB     | ########## | 100% 
pexpect-4.8.0        | 53 KB     | ########## | 100% 
jedi-0.17.0          | 780 KB    | ########## | 100% 
prompt-toolkit-3.0.8 | 248 KB    | ########## | 100% 
intel-openmp-2020.2  | 786 KB    | ########## | 100% 
jinja2-2.11.2        | 93 KB     | ########## | 100% 
qtconsole-4.7.7      | 96 KB     | ########## | 100% 
libxml2-2.9.10       | 1.2 MB    | ########## | 100% 
expat-2.2.10         | 153 KB    | ########## | 100% 
bleach-3.2.3         | 113 KB    | ########## | 100% 
jupyter_core-4.7.0   | 68 KB     | ########## | 100% 
markupsafe-1.1.1     | 29 KB     | ########## | 100% 
ptyprocess-0.7.0     | 17 KB     | ########## | 100% 
nbformat-5.1.2       | 68 KB     | ########## | 100% 
jupyter_client-6.1.7 | 77 KB     | ########## | 100% 
scipy-1.5.2          | 14.4 MB   | ########## | 100% 
testpath-0.4.4       | 85 KB     | ########## | 100% 
matplotlib-base-3.3. | 5.1 MB    | ########## | 100% 
entrypoints-0.3      | 12 KB     | ########## | 100% 
async_generator-1.10 | 39 KB     | ########## | 100% 
jsonschema-3.2.0     | 47 KB     | ########## | 100% 
zstd-1.4.5           | 619 KB    | ########## | 100% 
defusedxml-0.6.0     | 23 KB     | ########## | 100% 
mkl_fft-1.2.0        | 149 KB    | ########## | 100% 
pillow-8.1.0         | 626 KB    | ########## | 100% 
mkl-service-2.3.0    | 52 KB     | ########## | 100% 
qt-5.9.7             | 68.5 MB   | ########## | 100% 
importlib_metadata-2 | 11 KB     | ########## | 100% 
jupyter-1.0.0        | 6 KB      | ########## | 100% 
webencodings-0.5.1   | 19 KB     | ########## | 100% 
jupyter_console-6.2. | 26 KB     | ########## | 100% 
ipykernel-5.3.4      | 181 KB    | ########## | 100% 
lcms2-2.11           | 307 KB    | ########## | 100% 
pyparsing-2.4.7      | 59 KB     | ########## | 100% 
packaging-20.9       | 37 KB     | ########## | 100% 
argon2-cffi-20.1.0   | 46 KB     | ########## | 100% 
parso-0.8.1          | 69 KB     | ########## | 100% 
ca-certificates-2021 | 121 KB    | ########## | 100% 
qtpy-1.9.0           | 38 KB     | ########## | 100% 
notebook-6.2.0       | 4.1 MB    | ########## | 100% 
wcwidth-0.2.5        | 29 KB     | ########## | 100% 
libsodium-1.0.18     | 244 KB    | ########## | 100% 
lz4-c-1.9.3          | 186 KB    | ########## | 100% 
gstreamer-1.14.0     | 3.2 MB    | ########## | 100% 
icu-58.2             | 10.5 MB   | ########## | 100% 
importlib-metadata-2 | 35 KB     | ########## | 100% 
kiwisolver-1.3.0     | 80 KB     | ########## | 100% 
decorator-4.4.2      | 12 KB     | ########## | 100% 
pickleshare-0.7.5    | 13 KB     | ########## | 100% 
jupyterlab_widgets-1 | 109 KB    | ########## | 100% 
traitlets-4.3.3      | 140 KB    | ########## | 100% 
nbconvert-6.0.7      | 480 KB    | ########## | 100% 
dbus-1.13.18         | 504 KB    | ########## | 100% 
nbclient-0.5.1       | 58 KB     | ########## | 100% 
mkl_random-1.1.1     | 327 KB    | ########## | 100% 
libpng-1.6.37        | 278 KB    | ########## | 100% 
backcall-0.2.0       | 13 KB     | ########## | 100% 
nest-asyncio-1.4.3   | 11 KB     | ########## | 100% 
send2trash-1.5.0     | 14 KB     | ########## | 100% 
tornado-6.1          | 581 KB    | ########## | 100% 
mkl-2020.2           | 138.3 MB  | ########## | 100% 
blas-1.0             | 6 KB      | ########## | 100% 
pyqt-5.9.2           | 4.5 MB    | ########## | 100% 
zipp-3.4.0           | 15 KB     | ########## | 100% 
Preparing transaction: ...working... done
Verifying transaction: ...working... done
Executing transaction: ...working... done
Removing intermediate container 90b95cd94171
 ---> fac1661a9d81
Step 5/7 : RUN conda install -c bioconda nmrglue
 ---> Running in 79de2c7340b8
Collecting package metadata (current_repodata.json): ...working... done
Solving environment: ...working... failed with initial frozen solve. Retrying with flexible solve.
Solving environment: ...working... failed with repodata from current_repodata.json, will retry with next repodata source.
Collecting package metadata (repodata.json): ...working... done
Solving environment: ...working... done

## Package Plan ##

  environment location: /opt/conda

  added / updated specs:
    - nmrglue


The following packages will be downloaded:

    package                    |            build
    ---------------------------|-----------------
    nmrglue-0.7                |   py36h3010b51_0         293 KB  bioconda
    ------------------------------------------------------------
                                           Total:         293 KB

The following NEW packages will be INSTALLED:

  nmrglue            bioconda/linux-64::nmrglue-0.7-py36h3010b51_0


Proceed ([y]/n)? 

Downloading and Extracting Packages
nmrglue-0.7          | 293 KB    | ########## | 100% 
Preparing transaction: ...working... done
Verifying transaction: ...working... done
Executing transaction: ...working... done
Removing intermediate container 79de2c7340b8
 ---> cf9780ed0fdc
Step 6/7 : RUN mkdir /opt/notebooks
 ---> Running in 455cef17e36f
Removing intermediate container 455cef17e36f
 ---> b8622c20e2e2
Step 7/7 : CMD cat /proc/version && python3 --version
 ---> Running in 8286b7d8aecc
Removing intermediate container 8286b7d8aecc
 ---> 10f35019e5c5
Successfully built 10f35019e5c5
Successfully tagged condabuild:latest
(base) jodell@Elivagar miniconda % 
