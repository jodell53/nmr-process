2/4/2021
First complete version of a Docker container for processing 2D NMR files. 

Image built with miniconda3:4.9.2 and its version of Python 3, added numpy, scipy, matpllotlib and jupyter for notebook access via web browser (to allow viewing plots). Nmrglue 0.8 installed with pip.
data directory contains practice data sourced from nmrglue 0.8
file `build_details.txt` is excluded from the Docker image
